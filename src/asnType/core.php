<?php

namespace asnType;

class core
{

    /**
     * Determine if APC cache is enabled.
     */
    private $apc = false;

    /**
     * Our Array of Types.
     *
     * @var array
     */
    private $lists = false;
    
    /**
     * Our ASNTypes APC Key.
     *
     * @var string
     */
    private $apckey = "asnType";

    /**
     * List of files.
     *
     * @var array
     */
    private $files = array(

        'inactive',
        'business',
        'education',
        'hosting',
        'isp'

    );
    
    /** 
     * Constructor.
     */
    function __construct(){

        $this->testAPC();
        $this->load();

    }

    /**
     * Test to see if APC is functional.
     *
     * @return void
     */
    private function testAPC(){

        if(extension_loaded('apc') && ini_get('apc.enabled')){

            $this->apc = true;

        }

    }

    /**
     * Run our loader, and grab the lists.
     *
     * @return void
     */
    private function load(){

        if($this->apc){

            foreach($this->files as $type){

                if(apc_exists($this->apckey . $type)){

                    if($this->lists === false){

                        $this->lists = array();

                    }

                    $this->lists[$type] = json_decode(apc_fetch($this->apckey . $type), true);

                }

            }

        }

        if($this->lists === false){

            $this->lists = $this->importFromFile();
            
        }

    }

    /**
     * Import our lists from file.
     *
     * @return void
     */
    private function importFromFile(){



        $lists = array();

        foreach($this->files as $file){

            $fileContents = file_get_contents(__DIR__ . '/categories/' . $file . '.txt');

            $lists[$file] = explode(PHP_EOL, $fileContents);

            if($this->apc){

                apc_store($this->apckey . $file, $lists[$file]);
                
            }

        }

        return $lists;

    }

    /**
     * Check Type of ASN
     *
     * @param integer $asn
     * @return string type.
     */
    public function check(int $asn){

        foreach($this->files as $type){

            $result = array_search($asn, $this->lists[$type]);

            if($result){

                return $type;

            }

        }

    }

}